
# service_ssh_account

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with service_ssh_account](#setup)
    * [What service_ssh_account affects](#what-service_ssh_account-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with service_ssh_account](#beginning-with-service_ssh_account)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

service_ssh_account can be used to create users and optionally set up ssh keys and access restrictions.

It was originally developed for maintaining user accounts on various systems that could be used for key-based rsync between hosts. It can optionally manage any of the following for a list of users:
* private SSH key
* authorized_keys
* subnets restrictions for SSHd and iptables

## Setup

### What service_ssh_account affects **OPTIONAL**

If it's obvious what your module touches, you can skip this section. For example, folks can probably figure out that your mysql_instance module affects their MySQL instances.

If there's more that they should know about, though, this is the place to mention:

* Files, packages, services, or operations that the module will alter, impact, or execute.
* Dependencies that your module automatically installs.
* Warnings or other important notices.

### Setup Requirements **OPTIONAL**

The service_ssh_account module requires the following Puppet modules:
* puppetlabs/firewall
* puppetlabs/stdlib
* herculesteam-augeasproviders_ssh
* herculesteam-augeasproviders_pam

### Beginning with service_ssh_account

The very basic steps needed for a user to get the module up and running. This can include setup steps, if necessary, or it can be an example of the most basic use of the module.

## Usage

Include usage examples for common use cases in the **Usage** section. Show your users how to use your module to solve problems, and be sure to include code examples. Include three to five examples of the most important or common tasks a user can accomplish with your module. Show users how to accomplish more complex tasks that involve different types, classes, and functions working in tandem.

## Reference

This section is deprecated. Instead, add reference information to your code as Puppet Strings comments, and then use Strings to generate a REFERENCE.md in your module. For details on how to add code comments and generate documentation with Strings, see the Puppet Strings [documentation](https://puppet.com/docs/puppet/latest/puppet_strings.html) and [style guide](https://puppet.com/docs/puppet/latest/puppet_strings_style.html)

If you aren't ready to use Strings yet, manually create a REFERENCE.md in the root of your module directory and list out each of your module's classes, defined types, facts, functions, Puppet tasks, task plans, and resource types and providers, along with the parameters for each.

For each element (class, defined type, function, and so on), list:

  * The data type, if applicable.
  * A description of what the element does.
  * Valid values, if the data type doesn't make it obvious.
  * Default value, if any.

For example:

```
service_ssh_account::usermap:
   username1:
      authorized_keys:
         user1@127.0.0.1:
            type: ssh-rsa
            key: 'AAAAB3Nza[...]qXfdaQ=='
         user2@10.0.0.1:
            type: ssh-rsa
            key: 'AAAAB3Nza[...]qXfdaQ=='
      group: groupname1
      subnets:
         localhost: 127.0.0.1
         'remote site': 10.0.0.1/24
   username2::
      group: groupname2
      private_rsa_key_contents: |
        -----BEGIN RSA PRIVATE KEY-----
        MIIEowIBAAKCAQEA4wicTXofrIGeB3mhek6w3+kA9En2f/2b3RDqMm+XATEu4ajw
        XkuNVpJ5YOGKebSXfMaRPP1jqjq8eJpVbWGyHYJB6rzt2WzklecO
        -----END RSA PRIVATE KEY-----
```

## Limitations

In the Limitations section, list any incompatibilities, known issues, or other warnings.

## Development

In the Development section, tell other users the ground rules for contributing to your project and how they should submit their work.

## Release Notes/Contributors/Etc. **Optional**

If you aren't using changelog, put your release notes here (though you should consider using changelog). You can also add any additional sections you feel are necessary or important to include here. Please use the `## ` header.
