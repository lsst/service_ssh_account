# Create a user with optional ssh keys and subnet access
# 
# @summary Create a user with optional ssh keys and subnet access
#
# @example
#   service_ssh_account::service_ssh_user { 'namevar': }
define service_ssh_account::service_ssh_user(
  Optional[Hash] $authorized_keys = undef,
  Optional[String] $group = $name,  # ONLY USED FOR SSHD AUTH
  Optional[Boolean] $purge_ssh_keys = true,
  Optional[String] $private_rsa_key_contents = undef,
  Optional[Hash] $subnets = undef,
)
{

  $username = $name

  user { $username:
    ensure         => 'present',
    name           => $username,
    home           => "/home/${username}",
    purge_ssh_keys => $purge_ssh_keys,
  }
  file { "/home/${username}":
    ensure => 'directory',
    owner  => $username,
  }
  file { "/home/${username}/.ssh":
    ensure  => 'directory',
    owner   => $username,
    mode    => '0700',
    require => File["/home/${username}"],
  }
  if $authorized_keys {
    file { "/home/${username}/.ssh/authorized_keys":
      ensure  => present,
      owner   => $username,
      mode    => '0600',
      require => File["/home/${username}/.ssh"],
    }
    # DEFAULTS
    Ssh_authorized_key {
      user => $username,
      type => 'ssh-rsa',
    }
    $authorized_keys.each | $k, $v | {
      ssh_authorized_key { $k: * => $v }
    }
  }
  if $private_rsa_key_contents {
    file { "/home/${username}/.ssh/id_rsa":
      ensure  => 'present',
      content => $private_rsa_key_contents,
      owner   => $username,
      mode    => '0600',
      require => File["/home/${username}/.ssh"],
    }
  }
  if $subnets {
    # ADD FIREWALL RULE FOR SSH ACCESS
    $subnets.each | $location, $source_cidr | {
      firewall { "0022 allow SSH connections for ${name} from ${location}":
        dport    => 22,
        proto    => 'tcp',
        source   => $source_cidr,
        action   => 'accept',
        provider => 'iptables'
      }
    }

    # ADD SSHD_CONFIG BLOCK
    # DEFAULTS
    $config_defaults = {
      'notify' => Service[ sshd ],
    }
    $config_match_defaults = $config_defaults + { 'position' => 'before first match' }
    $match_condition = "User ${name}"
    # CONSTRUCT LIST OF USER/CIDR PAIRS
    $allow_users = $subnets.map | $location, $source_cidr | {
      "${name}@${source_cidr}"
    }
    $match_params = {
      'AllowGroups'           => $group,
      'AllowUsers'            => $allow_users,
      'PubkeyAuthentication'  => 'yes',
      'AuthenticationMethods' => 'publickey',
      'Banner'                => 'none',
#        'MaxSessions'           => '10',
      'X11Forwarding'         => 'no',
    }
    sshd_config_match {
      $match_condition :
      ;
      default: * => $config_match_defaults,
      ;
    }
    $match_params.each | $key, $val | {
      sshd_config {
        "${match_condition} ${key}" :
          key       => $key,
          value     => $val,
          condition => $match_condition,
        ;
        default: * => $config_defaults,
        ;
      }
    }
  }
}

