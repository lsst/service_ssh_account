# Create a set of users with optional ssh keys and subnet access
#
# @summary Create a set of users with optional ssh keys and subnet access
#
# @example
#   include service_ssh_account
class service_ssh_account (
  $usermap,
) {

  $usermap.each | $k, $v | {
    ::service_ssh_account::service_ssh_user{ $k: * => $v }
  }

}

